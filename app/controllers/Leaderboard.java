package controllers;

import play.*;
import play.mvc.*;
import utils.SocialComparator;
import utils.TalkativeComparator;
import java.util.*;
import models.*;

public class Leaderboard extends Controller {

	public static void social() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			Accounts.index();
		}
		List<User> social = User.findAll();
		Collections.sort(social, new SocialComparator());
		render(social);
	}
	
	public static void talkative() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			Accounts.index();
		}
		List<User> talkative = User.findAll();
		Collections.sort(talkative, new TalkativeComparator());
		render(talkative);
	}
}
