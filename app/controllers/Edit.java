package controllers;

import play.*;
import play.mvc.*;
import play.db.jpa.Blob;
import java.util.*;
import models.*;

public class Edit extends Controller {

	public static void index() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			Accounts.index();
		}
		User user = User.findById(Long.parseLong(userId));
		render(user);
	}

	public static void changeName(String firstName, String lastName) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.firstName = firstName;
		user.lastName = lastName;
		user.save();
		Logger.info("Name changed to: " + firstName + " " + lastName);
		index();
	}

	public static void changeAge(int age) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.age = age;
		user.save();
		Logger.info("Age changed to: " + age);
		index();
	}

	public static void changeNationality(String nationality) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.nationality = nationality;
		user.save();
		Logger.info("Nationality changed to: " + nationality);
		index();
	}

	public static void changeEmail(String email, String password) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.email = email;
		user.password = password;
		user.save();
		Logger.info("Email and password changed to: " + email + " " + password);
		index();
	}
}