package controllers;

import play.*;
import play.mvc.*;
import utils.MessageFromComparator;
import java.util.*;
import models.*;

public class Home extends Controller {

	public static void index() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			Accounts.index();
		}
		User user = User.findById(Long.parseLong(userId));
		render(user);
	}
	
	public static void byuser() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			Accounts.index();
		}
		User user = User.findById(Long.parseLong(userId));
		List<Message> byuser = user.inbox;
		Collections.sort(byuser, new MessageFromComparator());
		render(user, byuser);
	}
	
	public static void conversation() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			Accounts.index();
		}
		User user = User.findById(Long.parseLong(userId));
		ArrayList<ArrayList<Message>> conversations = new ArrayList<>();
		for (Friendship friendship : user.friendships) {
			conversations.add(getConversation(user, friendship.targetUser));
		}
		render(user, conversations);
	}

	public static void drop(Long id) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		User friend = User.findById(id);

		user.unfriend(friend);
		Logger.info("Dropping " + friend.email);
		index();
	}

	static ArrayList<Message> getConversation(User user, User friend) {
		ArrayList<Message> conversation = new ArrayList<>();
		for (Message message : user.outbox) {
			if (message.to == friend) {
				conversation.add(message);
			}
		}
		for (Message inMessage : user.inbox) {
			if (inMessage.from == friend) {
				conversation.add(inMessage);
			}
		}
		return conversation;
	}
}