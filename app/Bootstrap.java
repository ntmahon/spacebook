import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import models.User;
import play.db.jpa.Blob;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.libs.MimeTypes;
import play.test.Fixtures;

@OnApplicationStart
public class Bootstrap extends Job {
	public void doJob() throws FileNotFoundException {
	    Fixtures.deleteDatabase();
	    Fixtures.loadModels("data.yml");
		
	    String photoName = "public/images/homer.gif";
	    Blob blob = new Blob ();
	    blob.set(new FileInputStream(photoName), MimeTypes.getContentType(photoName));
	    User homer = User.findByEmail("homer@simpson.com");
	    homer.profilePicture = blob;
	    homer.save();
	    
	    String photoName1 = "public/images/marge.jpg";
	    Blob blob1 = new Blob ();
	    blob1.set(new FileInputStream(photoName1), MimeTypes.getContentType(photoName1));
	    User marge = User.findByEmail("marge@simpson.com");
	    marge.profilePicture = blob1;
	    marge.save();
	    
	    String photoName2 = "public/images/bart.gif";
	    Blob blob2 = new Blob ();
	    blob2.set(new FileInputStream(photoName2), MimeTypes.getContentType(photoName2));
	    User bart = User.findByEmail("bart@simpson.com");
	    bart.profilePicture = blob2;
	    bart.save();
	    
	    String photoName3 = "public/images/lisa.gif";
	    Blob blob3 = new Blob ();
	    blob3.set(new FileInputStream(photoName3), MimeTypes.getContentType(photoName3));
	    User lisa = User.findByEmail("lisa@simpson.com");
	    lisa.profilePicture = blob3;
	    lisa.save();
	    
	    String photoName4 = "public/images/maggie.gif";
	    Blob blob4 = new Blob ();
	    blob4.set(new FileInputStream(photoName4), MimeTypes.getContentType(photoName4));
	    User maggie = User.findByEmail("maggie@simpson.com");
	    maggie.profilePicture = blob4;
	    maggie.save();
	    
	    String photoName5 = "public/images/rock.png";
	    Blob blob5 = new Blob ();
	    blob5.set(new FileInputStream(photoName5), MimeTypes.getContentType(photoName5));
	    User rock = User.findByEmail("rocky@champ.com");
	    rock.profilePicture = blob5;
	    rock.save();
	    
	    String photoName6 = "public/images/hulk.jpg";
	    Blob blob6 = new Blob ();
	    blob6.set(new FileInputStream(photoName6), MimeTypes.getContentType(photoName6));
	    User hulk = User.findByEmail("hulk@hogan.com");
	    hulk.profilePicture = blob6;
	    hulk.save();
	    
	    String photoName7 = "public/images/stone.jpg";
	    Blob blob7 = new Blob ();
	    blob7.set(new FileInputStream(photoName7), MimeTypes.getContentType(photoName7));
	    User austin = User.findByEmail("stone@cold.com");
	    austin.profilePicture = blob7;
	    austin.save();
	    
	    String photoName8 = "public/images/clark.jpg";
	    Blob blob8 = new Blob ();
	    blob8.set(new FileInputStream(photoName8), MimeTypes.getContentType(photoName8));
	    User clark = User.findByEmail("clark@dailyplanet.com");
	    clark.profilePicture = blob8;
	    clark.save();
	    
	    String photoName9 = "public/images/union.jpg";
	    Blob blob9 = new Blob ();
	    blob9.set(new FileInputStream(photoName9), MimeTypes.getContentType(photoName9));
	    User jack = User.findByEmail("u.jack@hmss.co.uk");
	    jack.profilePicture = blob9;
	    jack.save();
	    
	    String photoName10 = "public/images/north.jpg";
	    Blob blob10 = new Blob ();
	    blob10.set(new FileInputStream(photoName10), MimeTypes.getContentType(photoName10));
	    User thomas = User.findByEmail("tnorth@hotmail.com");
	    thomas.profilePicture = blob10;
	    thomas.save();
	}
}